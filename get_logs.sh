#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2019 Free Software Foundation Europe <https://fsfe.org/contact>
#
# SPDX-License-Identifier: GPL-3.0-or-later

print_usage() {
  echo "Download and import webserver logs to Matomo"
  echo ""
  echo "Usage: ./get_logs.sh -t [normal|docker] -r root@server.com -f [<remote_log>|rp_hosts.csv] [-i <site_id>] [-s <site_hostname>] [-c <rp_container_name]"
  echo ""
  echo " -t normal|docker"
  echo "    normal: log file on remote host"
  echo "    docker: log file of a reverse proxy docker container"
  echo ""
  echo " -r <user@server>"
  echo "    Information about remote host"
  echo ""
  echo " -f <filename>"
  echo "    normal: webserver log file on remote host"
  echo "    docker: local file which contains information about proxied websites"
  echo ""
  echo " -i <number>"
  echo "    normal: id of this Matomo site"
  echo "    docker: N/A"
  echo ""
  echo " -s <hostname>"
  echo "    normal: hostname of this Matomo site"
  echo "    docker: N/A"
  echo ""
  echo " -c <container name>"
  echo "    normal: N/A"
  echo "    docker: name of the Docker container running the reverse proxy"
  echo ""
  echo " -h"
  echo "    Shows this help"
}

while getopts t:r:f:i:s:c:h OPT; do
  case $OPT in
    t)  TYPE=$OPTARG;;  # type: normal or docker
    r)  REMOTE=$OPTARG;;  # remote machine (e.g. root@example.com)
    f)  FILE=$OPTARG;;  # log file on remote host (normal), or local file with sitehosts (docker)
    i)  SITEID=$OPTARG;;  # site id in matomo (e.g. 4) (normal)
    s)  SITEHOST=$OPTARG;;  # site host (e.g. download.fsfe.org) (normal)
    c)  CONTAINER=$OPTARG;;  # docker container name of reverse proxy (docker)
    h)  print_usage; exit 0;;
    *)  echo "Unknown option: -$OPTARG"; print_usage; exit 1;;
  esac
done

# ----------------------------------------------------------------------
# STATIC VALUES
OPTIONS="/var/www/logimport/host_options.ini"
# Use same PHP mem limit as for root. Fixes updatetoken.php invoked by log_import
export PHP_MEMORY_LIMIT=256M

# ----------------------------------------------------------------------
# FUNCTIONS

# download log from remote server
function download {
  # 1: REMOTE, 2: FILE, 3: FILE_LOCAL

  rsync -avzq -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i /var/www/logimport/id_rsa" "$1":"$2" "$3"

  if [ $? -ne 0 ]; then
    echo "Log not found. Try gzipped form"
    rsync -avzq -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i /var/www/logimport/id_rsa" "$1":"$2".gz "$3".gz

    if [ $? -eq 0 ]; then
      echo "Gzipped file found. Unpacking it..."
      gunzip "$3".gz
    fi
  fi
}

# parse INI file. $1 is group (sitehost), $2 the specific key
function ini_option {
  cat <<EOF | python3
import configparser
parser = configparser.ConfigParser(allow_no_value=True)
parser.read("${OPTIONS}")
print(parser.get("$1", "$2"))
EOF
}

# import logs into matomo
function import {
  # 1: SITEID, 2: SITEHOST, 3: FILE_LOCAL

  # modify log file (FILE_LOCAL) according to filters (pre_filters)
  pre_filters=$(ini_option "$2" "pre_filters" 2> /dev/null)
  if [[ -n "${pre_filters}" ]]; then
    if [[ "${TYPE}" == "normal" ]]; then
      cat "${3}" | eval "${pre_filters}" > "${3}.tmp"
    else
      grep -E "^$2" "${3}" | eval "${pre_filters}" > "${3}.tmp"
    fi
  else
    # no filter applied, just copy full log to temporary file
    cp "${3}" "${3}.tmp"
  fi

  # import logs finally
  echo "Starting import of logs"
  /usr/bin/env python3 /var/www/html/misc/log-analytics/import_logs.py \
                      --idsite="$1" \
                      --hostname="$2" \
                      --exclude-path="*/robots.txt" \
                      $(ini_option "$2" "import_options" 2> /dev/null) \
                      --url="https://piwik.fsfe.org" \
                      --recorders=3 \
                      "${3}.tmp"

  # remove temporary file
  rm "${3}.tmp"

  # run archiver for site
  echo "Run archiver for site"
  /usr/bin/env php /var/www/html/console core:archive --force-idsites="${SITEID}"
}

# arguments to ssh command
function cssh {
  ssh -q -o ConnectTimeout=5 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i /var/www/logimport/id_rsa "$@"
}


# ----------------------------------------------------------------------
# SANTITY CHECKS

if [[ -z "${TYPE}" || -z "${FILE}" || -z "${REMOTE}" ]]; then
  echo "-t and/or -f and/or -r missing"
  print_usage
  exit 1
fi
if [[ ! -e "${OPTIONS}" ]]; then
  echo "[INFO] Options file \"${OPTIONS}\" does not exist"
fi
if [[ "${TYPE}" == "normal" ]]; then
  if [[ -z "${SITEID}" || -z "${SITEHOST}" ]]; then
    echo "-i and/or -s missing. Required in normal mode"
    print_usage
    exit 1
  fi
elif [[ "${TYPE}" == "docker" ]]; then
  if [[ -z "${CONTAINER}" ]]; then
    echo "-c missing. Required in docker mode"
    print_usage
    exit 1
  fi
else
  echo "Invalid type. Can only take normal or docker"
  print_usage
  exit 1
fi


# ----------------------------------------------------------------------
# DOWNLOAD AND IMPORT

if [[ "${TYPE}" == "normal" ]]; then    # TYPE = normal
  FILE_LOCAL=/var/www/logimport/"${SITEHOST}"-$(basename "${FILE}")-$(date +%s)

  # download log file
  download "${REMOTE}" "${FILE}" "${FILE_LOCAL}"

  # import log file
  import "${SITEID}" "${SITEHOST}" "${FILE_LOCAL}"

elif [[ "${TYPE}" == "docker" ]]; then  # TYPE = docker
  FILE_LOCAL=/var/www/logimport/"${CONTAINER}"-$(basename "${FILE}")-$(date +%s)

  # extract container's logfile of the latest 60m
  cmd="docker logs --since 60m ${CONTAINER}"

  # download logfile, convert to understandable format, and send to local file
  cssh "${REMOTE}" "$cmd" | sed -e $'s,\x1b\\[[0-9;]*[a-zA-Z],,g' -e 's/^nginx.[0-9]\s*| //g' > "${FILE_LOCAL}"

  # walk through hosts to extract from the container's log file
  while read -r line; do
    if echo "$line" | grep -qE "^\s*#"; then continue; fi
    SITEID=$(echo "$line" | cut -d"," -f1)
    SITEHOST=$(echo "$line" | cut -d"," -f2)

    # import log file
    echo
    echo "NOW IMPORTING: ${SITEHOST}"
    import "${SITEID}" "${SITEHOST}" "${FILE_LOCAL}"
  done < "${FILE}"
fi


# ----------------------------------------------------------------------
# Clean up

# delete downloaded file as we don't need it anymore
rm -v "${FILE_LOCAL}"
