#!/bin/bash

# SPDX-FileCopyrightText: 2019 Free Software Foundation Europe <https://fsfe.org/contact>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# This script cleans the Matomo DB to reset specific or all pages

DB_NAME=piwik
DB_USER=piwik
ID=$1

if [[ -z "$ID" ]]; then
  echo "Please provide a matomo site ID which to delete, or use \"all\" to clean all sites"
  exit 1
fi
if [[ ! ( "$ID" =~ ^[0-9]+$ ) && ! ( "$ID" == "all" ) ]]; then
  echo "First and only argument has to be a number (Matomo site ID) or \"all\""
  exit 1
fi

# Get user confirmation
read -p "Are you sure to delete one or all sites' entries from the database [y/n]? " -r
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
  echo "Aborted"
  exit 0
fi

if [[ "$ID" == "all" ]]; then
  # ALL SITES
  tables=
  for t in $(mysql -u "${DB_USER}" "${DB_NAME}" -e "SHOW TABLES;" | grep -E "_archive_numeric_.+"); do
    mysql -u "${DB_USER}" "${DB_NAME}" -e "DROP TABLE ${t}"
    tables="${tables} ${t}"
  done
  echo "Dropped tables: $(echo ${tables} | tr ' ' ,)"

  tables=
  for t in $(mysql -u "${DB_USER}" "${DB_NAME}" -e "SHOW TABLES;" | grep -E "_log_(visit|link_visit.*|conversion.*)"); do
    mysql -u "${DB_USER}" "${DB_NAME}" -e "TRUNCATE TABLE ${t}"
    tables="${tables} ${t}"
  done
  echo "Truncated tables: $(echo ${tables} | tr ' ' ,)"
else
  # CERTAIN ID
  tables=
  for t in $(mysql -u "${DB_USER}" "${DB_NAME}" -e "SHOW TABLES;" | grep -E "_archive_numeric_.+"); do
    mysql -u "${DB_USER}" "${DB_NAME}" -e "DELETE FROM ${t} WHERE idsite = ${ID}"
    tables="${tables} ${t}"
  done
  echo "Deleted idsite=${ID} from tables: $(echo ${tables} | tr ' ' ,)"

  tables=
  for t in $(mysql -u "${DB_USER}" "${DB_NAME}" -e "SHOW TABLES;" | grep -E "_log_(visit|link_visit.*|conversion.*)"); do
    mysql -u "${DB_USER}" "${DB_NAME}" -e "DELETE FROM ${t} WHERE idsite = ${ID}"
    tables="${tables} ${t}"
  done
  echo "Deleted idsite=${ID} from tables: $(echo ${tables} | tr ' ' ,)"
fi
